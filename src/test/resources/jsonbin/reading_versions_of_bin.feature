Feature: reading versions of bin

  Scenario Outline: create, update and read version

    Given create bin
    And updated bin
    When reading a "<version>"
    Then result of reading should be "<answer>"

    Examples:

      | version | answer |
      | valid   | 200    |
      | invalid | 422    |
      | latest  | 200    |
