Feature: delete bin

  Scenario Outline: is bin able to delete?

    Given bin parameters for delete are "<id>", "<secret key>"
    When deleting a bin
    Then result of deleting bin is "<answer>"

    Examples:
      | id      | secret key | answer |
      | valid   | valid      | 200    |
      | valid   | invalid    | 401    |
      | invalid | valid      | 422    |
      | valid   | null       | 401    |
      | null    | valid      | 404    |

