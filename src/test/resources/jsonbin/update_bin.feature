Feature: update bin

  Scenario Outline: is bin able to update?

    Given updated bin parameters are "<id>", "<content type>", "<is empty>", "<secret key>", "<versioning>", "<privacy>"
    When updating a bin
    Then result of updating bin is "<answer>"

    Examples:
      | id      | content type     | is empty | secret key | versioning | privacy | answer |
      | valid   | application/json | false    | valid      | true       | true    | 200    |
      | valid   | application/json | false    | valid      | false      | true    | 200    |
      | valid   | application/json | false    | valid      | null       | true    | 200    |
      | valid   | application/json | false    | invalid    | true       | true    | 401    |
      | valid   | application/json | false    | null       | true       | true    | 401    |
      | valid   | null             | false    | valid      | true       | true    | 422    |
      | valid   | application/text | false    | valid      | true       | true    | 422    |
      | valid   | application/json | true     | valid      | true       | true    | 422    |
      | valid   | application/json | not json | valid      | true       | true    | 400    |
      | invalid | application/json | false    | valid      | true       | true    | 422    |
      | null    | application/json | false    | valid      | true       | true    | 404    |
      | valid   | application/json | false    | valid      | true       | false   | 200    |
      | valid   | application/json | false    | invalid    | true       | false   | 200    |
      | valid   | application/json | false    | null       | true       | false   | 200    |
#server returns 200 (we cannot disable versioning on public bins)
      | valid   | application/json | false    | valid      | false      | false   | 400    |
      | valid   | application/json | false    | valid      | null       | false   | 400    |





