Feature: create, delete and read bin

  Scenario Outline: are we able to read or update bin after delete?

    Given creating bin
    And deleting bin
    When "<read or update>" deleted bin
    Then answer should be "<answer>"

    Examples:
      | read or update | answer |
      | read           | 404    |
      | update         | 404    |
