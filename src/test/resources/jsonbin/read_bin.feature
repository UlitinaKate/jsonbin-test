Feature: read bin

  Scenario Outline: is bin readable?

    Given bin parameters are "<id>", "<secret key>", "<privacy>"
    When reading a bin
    Then result of reading bin is "<answer>"

    Examples:
      | id      | secret key | privacy | answer |
      | valid   | valid      | true    | 200    |
      | valid   | invalid    | true    | 401    |
      | invalid | valid      | true    | 422    |
      | valid   | null       | true    | 401    |
      | null    | valid      | true    | 404    |
      | valid   | valid      | false   | 200    |
      | valid   | invalid    | false   | 200    |
      | invalid | valid      | false   | 422    |
      | valid   | null       | false   | 200    |
      | null    | valid      | false   | 404    |

