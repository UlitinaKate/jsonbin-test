Feature: Is bin created?

  Scenario Outline: are we able to create a bin?

    Given parameters are "<content type>", "<secret key>", "<is empty>", "<collection id>", "<private>", "<name>"
    When creating a bin
    Then result is "<answer>"

    Examples:
      | content type     | secret key | is empty | collection id | private | name  | answer |
      | application/json | valid      | false    | null          | null    | null  | 200    |
      | application/json | valid      | false    | valid         | false   | any   | 200    |
      | application/json | valid      | false    | valid         | true    | any   | 200    |
      | null             | valid      | false    | null          | null    | null  | 422    |
      | application/text | valid      | false    | null          | null    | null  | 422    |
      | application/json | null       | false    | null          | null    | null  | 401    |
      | application/json | invalid    | false    | null          | null    | null  | 401    |
      | application/json | valid      | true     | null          | null    | null  | 422    |
      | application/json | valid      | false    | invalid       | null    | null  | 422    |
      | application/json | valid      | false    | null          | null    | long  | 422    |
      #should be 422, but server returns 200
      | application/json | valid      | false    | null          | null    | empty | 422    |
      # not from spec
      | application/json | valid      | not json | null          | null    | null  | 400    |
