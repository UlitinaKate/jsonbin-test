package jsonbin;

public class JsonBinConstants {
  public static final String BIN_URL = "https://api.jsonbin.io/b/";
  public static final String COLLECTION_URL = "https://api.jsonbin.io/c/";
  public static final String JSON_TYPE = "application/json";
  public static final String JSON_BODY = "{\"body\":\"body\"}";
  public static final String LONG_NAME = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345679";
  public static final String TEST_NAME = "012345678901234ui5678901234567845uiop[]asdfghjdkl;'zxcvbnm,./`~!@#$%^&*()_+={}|?><2345678901234567890123456789012345678901234567";
  public static final String INVALID_VALUE = "invalid";
}
