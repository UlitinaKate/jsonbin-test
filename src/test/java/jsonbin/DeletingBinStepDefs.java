package jsonbin;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.ValidatableResponse;

public class DeletingBinStepDefs {

  private String secretKey;
  private String binID;
  private final String actualSecretKey = ConfProperties.getProperty("secretKey");
  private Bin bin;
  private ValidatableResponse response;

  @Given("bin parameters for delete are {string}, {string}")
  public void binParametersForDeleteAre(String isIdValid, String isSecretKeyValid) {
    bin = new Bin(JsonBinConstants.JSON_TYPE, actualSecretKey, JsonBinConstants.JSON_BODY, null, null, null);
    binID = bin.sendRequestAndGetBinID();
    secretKey = isSecretKeyValid.equals("valid") ? actualSecretKey : isSecretKeyValid.equals("invalid") ? JsonBinConstants.INVALID_VALUE : null;
    if (isIdValid.equals("invalid")) {
      binID = JsonBinConstants.INVALID_VALUE;
    }
    else if (isIdValid.equals("null")) {
      binID = "";
    }
  }

  @When("deleting a bin")
  public void deletingABin(){
    response = bin.sendRequestForDeleting(binID, secretKey);
  }

  @Then("result of deleting bin is {string}")
  public void resultOfDeletingBinIs(String expectedAnswer) {
    response.statusCode(Integer.parseInt(expectedAnswer));
  }
}
