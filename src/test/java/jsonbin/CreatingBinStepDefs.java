package jsonbin;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

import java.net.URI;

public class CreatingBinStepDefs {
  private Bin bin;
  private ValidatableResponse response;

  @Given("parameters are {string}, {string}, {string}, {string}, {string}, {string}")
  public void parametersAre(String contentType, String typeOfSecretKey, String isEmpty, String typeOfCollectionID, String isPrivate, String typeOfName) {
    String secretKey = typeOfSecretKey.equals("valid") ? ConfProperties.getProperty("secretKey") : "invalid".equals(typeOfSecretKey) ? JsonBinConstants.INVALID_VALUE : null;
    String body = "true".equals(isEmpty) ? "" : "false".equals(isEmpty) ? JsonBinConstants.JSON_BODY : "text";
    String collectionID = typeOfCollectionID.equals("valid") ? createCollectionAndGetID() : typeOfCollectionID.equals("invalid") ? JsonBinConstants.INVALID_VALUE : null;
    String name = typeOfName.equals("empty") ? "" : typeOfName.equals("any") ? JsonBinConstants.TEST_NAME : typeOfName.equals("long") ? JsonBinConstants.LONG_NAME : null;
    bin = new Bin(contentType, secretKey, body, collectionID, isPrivate, name);
  }

  @When("creating a bin")
  public void creatingBin() {
    response = bin.sendRequestForBinCreation();
  }

  @Then("result is {string}")
  public void result(String expectedAnswer) {
    response.statusCode(Integer.parseInt(expectedAnswer));
  }


  private String createCollectionAndGetID(){
    return RestAssured.given()
            .contentType(ContentType.JSON)
            .header("secret-key", ConfProperties.getProperty("secretKey"))
            .body("{\"name\": \"Kate's collection\"}")
            .post(URI.create(JsonBinConstants.COLLECTION_URL))
            .then().extract().body().path("id");
  }
}
