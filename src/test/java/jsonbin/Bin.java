package jsonbin;

import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import java.net.URI;

public class Bin {

  private final String contentType;
  private final String secretKey;
  private String body;
  private final String collectionID;
  private final String isPrivate;
  private final String name;
  private String id;
  private String version;

  public Bin(String contentType, String secretKey, String body, String collectionID, String isPrivate, String name) {
    this.contentType = contentType;
    this.secretKey = secretKey;
    this.body = body;
    this.collectionID = collectionID;
    this.isPrivate = isPrivate;
    this.name = name;
  }

  public ValidatableResponse sendRequestForBinCreation() {

    RequestSpecification builder = RestAssured.given();

    if (contentType != null) {
      builder = builder.contentType(contentType);
    }
    if (secretKey != null) {
      builder = builder.header("secret-key", secretKey);
    }
    if (collectionID != null) {
      builder = builder.header("collection-id", collectionID);
    }
    if (isPrivate != null) {
      builder = builder.header("private", isPrivate);
    }
    if (name != null) {
      builder = builder.header("name", name);
    }

    builder = builder.body(body.getBytes());
    ValidatableResponse response = builder.post(URI.create(JsonBinConstants.BIN_URL)).then();

    id = response.extract().body().path("id");
    return response;
  }

  public String sendRequestAndGetBinID() {
    sendRequestForBinCreation();
    return id;
  }

  public ValidatableResponse sendRequestForUpdateBin(String versioning, String newBody, String binID, String secretKey, String contentType) {

    RequestSpecification builder = RestAssured.given();
    if (id == null) {
      throw new IllegalStateException("id is null");
    }
    if (contentType != null) {
      builder = builder.contentType(contentType);
    }
    if (secretKey != null) {
      builder = builder.header("secret-key", secretKey);
    }
    if (versioning != null) {
      builder = builder.header("versioning", versioning);
    }
    builder = builder.body(newBody.getBytes());

    ValidatableResponse response = builder.put(URI.create(JsonBinConstants.BIN_URL + binID)).then();

    body = newBody;
    Object version = response.extract().body().path("version");
    this.version = version != null ? version.toString() : null;

    return response;
  }

  public void sendUpdateAndGetVersion() {
    sendRequestForUpdateBin("true", body, id, ConfProperties.getProperty("secretKey"), contentType);
  }

  public ValidatableResponse sendRequestForReadindBin(String binID, String secretKey, String versioning) {
    RequestSpecification builder = RestAssured.given();
    if (secretKey != null) {
      builder = builder.header("secret-key", secretKey);
    }

    String versionType = versioning.equals("valid") ?  "/" + version : versioning.equals("invalid") ? "/" +JsonBinConstants.INVALID_VALUE : versioning.equals("latest") ? "/latest" : "";
    String url = JsonBinConstants.BIN_URL + binID + versionType;

    return builder.get(URI.create(url)).then();
  }

  public ValidatableResponse sendRequestForDeleting(String binID, String secretKey) {

    RequestSpecification builder = RestAssured.given();
    if (secretKey != null) {
      builder = builder.header("secret-key", secretKey);
    }
    return builder.delete(URI.create(JsonBinConstants.BIN_URL + binID)).then();
  }

  public String getBody() {
    return body;
  }

  public String getId() {
    return id;
  }
}