package jsonbin;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.ValidatableResponse;

public class UpdatingBinStepDefs {

  private String versioning;
  private Bin bin;
  private String binID;
  private String secretKey;
  private String contentType;
  private String body;
  private ValidatableResponse response;

  @Given("updated bin parameters are {string}, {string}, {string}, {string}, {string}, {string}")
  public void updatedBinParametersAre(String isIdValid, String contentType, String isEmpty, String isSecretKeyValid, String versioning, String isPrivate) {
    this.contentType = contentType;
    secretKey = isSecretKeyValid.equals("valid") ? ConfProperties.getProperty("secretKey") : "invalid".equals(isSecretKeyValid) ? JsonBinConstants.INVALID_VALUE : null;
    String jsonForUpdate = "{\"body\":\"new body\"}";
    body = "true".equals(isEmpty) ? "" : "false".equals(isEmpty) ? jsonForUpdate : "text";
    this.versioning = versioning;
    bin = new Bin(JsonBinConstants.JSON_TYPE, ConfProperties.getProperty("secretKey"), JsonBinConstants.JSON_BODY, null, isPrivate, null);
    binID = bin.sendRequestAndGetBinID();
    if (isIdValid.equals("invalid")) {
      binID = JsonBinConstants.INVALID_VALUE;
    }
    else if (isIdValid.equals("null")) {
      binID = "";
    }
  }

  @When("updating a bin")
  public void updatingABin() {
    response = bin.sendRequestForUpdateBin(versioning, body, binID, secretKey, contentType);
  }

  @Then("result of updating bin is {string}")
  public void resultOfUpdatingBinIs(String expectedAnswer) {
    response.statusCode(Integer.parseInt(expectedAnswer));
  }
}
