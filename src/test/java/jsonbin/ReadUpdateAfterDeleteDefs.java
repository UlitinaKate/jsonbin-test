package jsonbin;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

public class ReadUpdateAfterDeleteDefs {

  private final String actualSecretKey = ConfProperties.getProperty("secretKey");
  private final Bin bin = new Bin(JsonBinConstants.JSON_TYPE, actualSecretKey, JsonBinConstants.JSON_BODY, null, null, null);
  private String binID;
  private ValidatableResponse response;

  @Given("creating bin")
  public void creatingBin() {
    binID = bin.sendRequestAndGetBinID();
  }

  @And("deleting bin")
  public void deletingBin() {
    bin.sendRequestForDeleting(binID, actualSecretKey);
  }

  @When("{string} deleted bin")
  public void deletedBin(String readOrUpdate) {
    if (readOrUpdate.equals("read")) {
      response = bin.sendRequestForReadindBin(binID, actualSecretKey, "false");
    }
    else {
      response = bin.sendRequestForUpdateBin("false", "{\"body\":\"new body\"}", binID, actualSecretKey, ContentType.JSON.toString());
    }
  }

  @Then("answer should be {string}")
  public void answerShouldBe(String expectedAnswer) {
    response.statusCode(Integer.parseInt(expectedAnswer));
  }
}
