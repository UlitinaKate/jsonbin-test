package jsonbin;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.ValidatableResponse;
import org.hamcrest.Matchers;

public class ReadingVersionsStepDefs {

  private final String actualSecretKey = ConfProperties.getProperty("secretKey");
  private final Bin bin = new Bin(JsonBinConstants.JSON_TYPE, actualSecretKey, "{\"body\":\"body1\"}", null, null, null);
  ValidatableResponse response;

  @Given("create bin")
  public void createBin() {
    bin.sendRequestForBinCreation();
  }

  @And("updated bin")
  public void updatedBin() {
    bin.sendUpdateAndGetVersion();
  }

  @When("reading a {string}")
  public void readingVersion(String versioning) {

    response = bin.sendRequestForReadindBin(bin.getId(), actualSecretKey, versioning);
  }

  @Then("result of reading should be {string}")
  public void resultOfReadingShouldBe(String expectedAnswer) {
    response.statusCode(Integer.parseInt(expectedAnswer));
    if (response.extract().statusCode() == 200) {
      response.body(Matchers.equalTo(bin.getBody()));
    }
  }
}
