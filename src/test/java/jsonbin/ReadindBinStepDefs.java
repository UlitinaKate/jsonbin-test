package jsonbin;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.ValidatableResponse;

public class ReadindBinStepDefs {

  private String binID;
  private final String actualSecretKey = ConfProperties.getProperty("secretKey");
  private Bin bin;
  private String secretKey;
  private ValidatableResponse response;

  @Given("bin parameters are {string}, {string}, {string}")
  public void parametersAre(String isIdValid, String isSecretKeyValid, String isPrivate) {
    bin = new Bin(JsonBinConstants.JSON_TYPE, actualSecretKey, JsonBinConstants.JSON_BODY, null, isPrivate, null);
    binID = bin.sendRequestAndGetBinID();
    secretKey = isSecretKeyValid.equals("valid") ? actualSecretKey : isSecretKeyValid.equals("invalid") ? JsonBinConstants.INVALID_VALUE : null;
    if (isIdValid.equals("invalid")) {
      binID = JsonBinConstants.INVALID_VALUE;
    }
    else if (isIdValid.equals("null")) {
      binID = "";
    }
  }

  @When("reading a bin")
  public void readingABin(){
    response = bin.sendRequestForReadindBin(binID, secretKey, "false");
  }

  @Then("result of reading bin is {string}")
  public void resultOfReadingPrivateBinIs(String expectedAnswer) {
    response.statusCode(Integer.parseInt(expectedAnswer));
  }
}
